#!/bin/bash
[ -n "$DEBUG" ] && set -x

log (){
    echo -e "${@}" >&2
}

cleanup () {
    log "Error caught on line $(caller)"

    kill -s SIGTERM $!
    exit 1
}

fix_user_rights(){
    trap 'exit 1;' ERR

    [ -n "$UID" ]

    local LUID=${UID:-$(id -u user)}
    local LGID=root

    if [ "${LUID}" -ne "$UID" ]; then
        usermod -u "${LUID}" user
        chown -Rh "${LUID}" /home/user
    fi

    groupmod -g "${LGID}" user 2>/dev/null || :
    chown -Rh :"${LGID}" /home/user
}

function set_vnc_password(){
    trap cleanup SIGINT SIGTERM ERR

    if [ -n "$VNC_PASSWORD" ];
    then
        log "-----| set vnc password"
        mkdir -p "$HOME/.vnc"

        [ -f "$HOME"/.vnc/passwd ] && rm -f "$PASSWD_PATH"

        echo "$VNC_PASSWORD" | vncpasswd -f >> "$PASSWD_PATH"
        chmod 600 "$PASSWD_PATH"
        log "vnc password changed"
    else
        log "vnc password not changed (export VNC_PASSWORD to change)"
    fi
}

function start_novnc(){
    trap cleanup SIGINT SIGTERM ERR

    [ -n "$LOGDIR" ]  # LOGDIR
    [ -n "$NO_VNC_PORT" ]
    [ -n "$VNC_PORT" ]

    mkdir -p "$LOGDIR"

    echo -e "-----| start noVNC"

    possible_binaries=(
        /usr/share/novnc/utils/launch.sh
        /usr/bin/novnc_server
    )
    for binary in "${possible_binaries[@]}"; do
        if [ -x "$binary" ]; then
            NOVNC_BINARY=$binary
            break
        fi
    done
    [ -n "$NOVNC_BINARY" ]

    local COMMAND=(
        "$NOVNC_BINARY"
        --vnc localhost:"$VNC_PORT"
        --listen "$NO_VNC_PORT"
    )
    echo "$(date): running: ${COMMAND[*]}" >> "$LOGDIR"/no_vnc_startup.log
    "${COMMAND[@]}" &>> "$LOGDIR"/no_vnc_startup.log &
    export VNC_PID=$!
}

function start_vnc(){
    trap cleanup SIGINT SIGTERM ERR

    [ -n "$DISPLAY" ]
    [ -n "$LOGDIR" ]
    [ -n "$VNC_DEPTH" ]
    [ -n "$VNC_GEOMETRY" ]

    log -e "-----| start VNC"
    mkdir -p "$LOGDIR"

    log "remove old vnc locks to be a reattachable container"
    vncserver -kill "$DISPLAY" &> "$HOME"/vnc_startup.log \
        || rm -rfv /tmp/.X*-lock /tmp/.X11-unix &>> "$HOME"/vnc_startup.log \
        || echo "no locks present"

    local COMMAND=(
        vncserver "$DISPLAY"
        -depth "$VNC_DEPTH"
        -geometry "$VNC_GEOMETRY"
    )
    echo "$(date): running: ${COMMAND[*]}" >> "$LOGDIR"/no_vnc_startup.log
    "${COMMAND[@]}" &>> "$LOGDIR"/vnc_startup.log &
}

function start_xfce(){
    trap cleanup SIGINT SIGTERM ERR

    [ -n "$DISPLAY" ]
    [ -n "$LOGDIR" ]


    log "-----| disable screensaver and power management"
    xset -dpms &
    xset s noblank &
    xset s off &

    log "-----| start window manager (xfce)"
    /usr/bin/startxfce4 --replace > "$LOGDIR"/xfce.log &
}


function log_connection_details(){
    trap cleanup SIGINT SIGTERM ERR
    ## log connect options
    echo -e "\nVNCSERVER started on DISPLAY=$DISPLAY \n\t=> connect via VNC viewer with localhost:$VNC_PORT"
    echo -e "\nnoVNC HTML client started:\n\t=> connect via http://localhost:$NO_VNC_PORT/?password=$VNC_PASSWORD\n"
}

trap cleanup SIGINT SIGTERM ERR

if [[ $# -gt 0 ]];
then
    while [[ $# -gt 0 ]]; 
    do
        $1
        shift
    done
else
    #fix_user_rights
    set_vnc_password
    start_novnc
    start_vnc
    start_xfce

    if [ -n "$DEBUG" ];
    then
        echo -e "-----| showing logfiles"
        sleep 2
        for a in "$LOGDIR"/*; do tail -n 50 "$a"; done
    fi

    wait $VNC_PID
fi


