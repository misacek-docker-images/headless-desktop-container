
.DEFAULT_GOAL: help

CONTAINER_REGISTRY ?= docker-registry.upshift.redhat.com
CONTAINER_REGISTRY_USER ?= user
CONTAINER_REGISTRY_PASSWORD ?= password
CONTAINER_REGISTRY_JSON_TOKEN ?= not-really-working-token
CONTAINER_REGISTRY_CONFIG_DIR ?= ~/.docker/$(CONTAINER_REGISTRY_USER)@$(CONTAINER_REGISTRY)
CONTAINER_IMAGE_PREFIX ?= mnovacek
CONTAINER_IMAGE_BASENAME ?= headless-desktop-container
CONTAINER_UID ?= 10000

# This is ansible-playbook connection type: buildah or docker
CONTAINER_ENGINE ?= docker

NO_VNC_PORT ?= 6901
VNC_PORT ?= 5901
VNC_DEPTH ?= 24
VNC_GEOMETRY ?= 1600x1024

# ----

CENTOS_BASE_IMAGE := docker.io/library/centos:7
CENTOS_BASE_IMAGE_NAME := centos-7
UBUNTU_BASE_IMAGE := docker.io/library/ubuntu:18.04
UBUNTU_BASE_IMAGE_NAME := ubuntu-18-04

# command to start base container images
BASECONT_COMMAND := ansible-playbook -i localhost, -c local -v base-start.yml -e container_engine=$(CONTAINER_ENGINE)
INSTALL_COMMAND := ansible-playbook -c $(CONTAINER_ENGINE) install.yml
COMMIT_COMMAND := ansible-playbook -i localhost, -c local -v commit.yml -e container_engine=$(CONTAINER_ENGINE)
CLEAN_COMMAND := ansible-playbook -i localhost, -c local -v clean.yml
PUSH_COMMAND := ansible-playbook -i localhost, -c local -v push.yml -e container_engine=$(CONTAINER_ENGINE)
PUSH_COMMAND += -e container_image_basename=$(CONTAINER_IMAGE_BASENAME)
PUSH_COMMAND += -e container_image_prefix=$(CONTAINER_IMAGE_PREFIX)
PUSH_COMMAND += -e container_registry=$(CONTAINER_REGISTRY)
PUSH_COMMAND += -e container_registry_config_dir=$(CONTAINER_REGISTRY_CONFIG_DIR)
PUSH_COMMAND += -e container_registry_json_token=$(CONTAINER_REGISTRY_JSON_TOKEN)
PUSH_COMMAND += -e container_registry_password=$(CONTAINER_REGISTRY_PASSWORD)
PUSH_COMMAND += -e container_registry_user=$(CONTAINER_REGISTRY_USER)

RUN_COMMAND := ansible-playbook -i localhost, -c local -v run.yml
RUN_COMMAND += -e container_registry=$(CONTAINER_REGISTRY)
RUN_COMMAND += -e container_image_prefix=$(CONTAINER_IMAGE_PREFIX)
RUN_COMMAND += -e container_image_basename=$(CONTAINER_IMAGE_BASENAME)

ifeq ($(CONTAINER_ENGINE),docker)
CONTAINER_EXEC := docker exec
endif
ifeq ($(CONTAINER_ENGINE),buildah)
CONTAINER_EXEC := buildah run
endif

help-variables:
	@echo Available variables:
	@echo
	@echo "    DEBUG:\n\tenter container interactively instead of running it"
	@echo "    DRY_RUN: \n\techo commands to be run instead of running them"
	@echo
	@echo "    CONTAINER_ENGINE"
	@echo "    CONTAINER_IMAGE_BASENAME"
	@echo "    CONTAINER_IMAGE_PREFIX"
	@echo "    CONTAINER_REGISTRY"
	@echo "    CONTAINER_REGISTRY_CONFIG_DIR"
	@echo "    CONTAINER_REGISTRY_PASSWORD"
	@echo "    CONTAINER_REGISTRY_USER"
	@echo "    CONTAINER_UID"
	@echo
	@echo "    NO_VNC_PORT"
	@echo "    VNC_DEPTH"
	@echo "    VNC_GEOMETRY"
	@echo "    VNC_PORT"

	@echo

help: help-variables
	@echo Available targets:
	@echo
ifdef HAS_COLUMN
ifdef COLUMN_HAS_TABLE_WRAP
	@cat Makefile* | grep '^.PHONY: .* #' | sed 's/\.PHONY: \(.*\) # \(.*\)/\1|\2/' | column -t -s '|' -W 2 $(SORTED_OUTPUT)
else
	@cat Makefile* | grep '^.PHONY: .* #' | sed 's/\.PHONY: \(.*\) # \(.*\)/\1|\2/' | column -t -s '|' $(SORTED_OUTPUT)
endif
else
	@cat Makefile* | grep '^.PHONY: .* #' | sed 's/\.PHONY: \(.*\) # \(.*\)/\1 - \2/' $(SORTED_OUTPUT)
endif
.PHONY: help # this text

#COMMIT_ARGS := --author "Michal Novacek <mnovacek@redhat.com>"
#COMMIT_ARGS += --change "ENTRYPOINT [\"/entrypoint.sh\"]"
#COMMIT_ARGS += --change "ENV HOME=/home/user"
#COMMIT_ARGS += --change "ENV LOGDIR=/home/user/logs"
#COMMIT_ARGS += --change "WORKDIR /home/user"
#
#COMMIT_ARGS += --change "ENV NO_VNC_PORT=$(NO_VNC_PORT)"
#COMMIT_ARGS += --change "EXPOSE $(NO_VNC_PORT)"
#
#COMMIT_ARGS += --change "ENV VNC_PORT=$(VNC_PORT)"
#COMMIT_ARGS += --change "EXPOSE $(VNC_PORT)"
#
#COMMIT_ARGS += --change "ENV VNC_DEPTH=$(VNC_DEPTH)"
#COMMIT_ARGS += --change "ENV VNC_GEOMETRY=$(VNC_GEOMETRY)"
#
#COMMIT_ARGS += --change "ENV DISPLAY=:1"
#COMMIT_ARGS += --change "USER $(CONTAINER_UID)"
#
#DOCKER_RUN_ARGS := --env DEBUG=$(DEBUG) -p $(NO_VNC_PORT):$(NO_VNC_PORT) -p $(VNC_PORT):$(VNC_PORT)
#
#ifdef VNC_PASSWORD
#DOCKER_RUN_ARGS += --env VNC_PASSWORD=$(VNC_PASSWORD)
#endif
#DOCKER_RUN_ARGS += --env VNC_DEPTH=$(VNC_DEPTH)
#DOCKER_RUN_ARGS += --env VNC_GEOMETRY=$(VNC_GEOMETRY)
#DOCKER_RUN_ARGS += --env NO_VNC_PORT=$(NO_VNC_PORT)
#
#ifdef DEBUG
#DOCKER_RUN_ARGS += -ti --entrypoint /bin/bash --user root
#else
#DOCKER_RUN_ARGS +=
#endif

ifdef DRY_RUN
ECHO := @echo
else
ECHO :=
endif

galaxy-requirements:
	# $@: ansible-playbook -i localhost, -c local -v galaxy-requirements.yml
	@$(ECHO) ansible-playbook -i localhost, -c local -v galaxy-requirements.yml
	@echo '$(shell date) Makefile: success: $@ '
.PHONY: galaxy-requirements

prepare_localhost:
	# $@: ansible-playbook -i localhost, -c local -v pre.yml
	@$(ECHO) ansible-playbook -i localhost, -c local -v pre.yml
	@echo '$(shell date) Makefile: success: $@ '
.PHONY: prepare_localhost  # runs preparation tasks on localhost (python-docker, galaxy roles)

start-base-centos: BASECONT_COMMAND += -e desired_state=started
start-base-centos: BASECONT_COMMAND += -e base_image=$(CENTOS_BASE_IMAGE)
start-base-centos: BASECONT_COMMAND += -e base_image_name=$(CENTOS_BASE_IMAGE_NAME)
start-base-centos: prepare_localhost galaxy-requirements
	#
	# $@: start $(CENTOS_BASE_IMAGE) container base image
	# $@: $(BASECONT_COMMAND)
	#
	@$(ECHO) $(BASECONT_COMMAND)
	@echo '$(shell date) Makefile: success: $@ '
.PHONY: base-centos

start-base-ubuntu: BASECONT_COMMAND += -e desired_state=started
start-base-ubuntu: BASECONT_COMMAND += -e base_image=$(UBUNTU_BASE_IMAGE)
start-base-ubuntu: BASECONT_COMMAND += -e base_image_name=$(UBUNTU_BASE_IMAGE_NAME)
start-base-ubuntu: prepare_localhost galaxy-requirements
	#
	# $@: start $(UBUNTU_BASE_IMAGE) container base image
	# $@: $(BASECONT_COMMAND)
	#
	@$(ECHO) $(BASECONT_COMMAND)
	@$(ECHO) $(CONTAINER_EXEC) $(UBUNTU_BASE_IMAGE_NAME) apt-get update
	@$(ECHO) $(CONTAINER_EXEC) $(UBUNTU_BASE_IMAGE_NAME) apt-get -y install python3
	@echo '$(shell date) Makefile: success: $@ '
.PHONY: base-ubuntu

start-base: start-base-ubuntu start-base-centos
	@echo '$(shell date) Makefile: success: $@ '
.PHONY: start-base  # start bases for building conatainer images


stop-base-centos: BASECONT_COMMAND += -e desired_state=absent
stop-base-centos: BASECONT_COMMAND += -e base_image=$(CENTOS_BASE_IMAGE)
stop-base-centos: BASECONT_COMMAND += -e base_image_name=$(CENTOS_BASE_IMAGE_NAME)
stop-base-centos: prepare_localhost
	#
	# $@: stop $(CENTOS_BASE_IMAGE) container base image
	# $@: $(BASECONT_COMMAND)
	#
	@$(ECHO) $(BASECONT_COMMAND)
	@echo '$(shell date) Makefile: success: $@ '
.PHONY: base-centos

stop-base-ubuntu: BASECONT_COMMAND += -e desired_state=absent
stop-base-ubuntu: BASECONT_COMMAND += -e base_image=$(UBUNTU_BASE_IMAGE)
stop-base-ubuntu: BASECONT_COMMAND += -e base_image_name=$(UBUNTU_BASE_IMAGE_NAME)
stop-base-ubuntu: prepare_localhost
	#
	# $@: stop $(UBUNTU_BASE_IMAGE) container base image
	# $@: $(BASECONT_COMMAND)
	#
	@$(ECHO) $(BASECONT_COMMAND)
	@echo '$(shell date) Makefile: success: $@ '
.PHONY: stop-base-ubuntu

stop-base: stop-base-ubuntu stop-base-centos
	@echo '$(shell date) Makefile: success: $@ '
.PHONY: stop-base

install-centos: INSTALL_COMMAND += -i $(CENTOS_BASE_IMAGE_NAME),
install-centos:
	#
	# $@: install vnc, novnc and xfce on $(CENTOS_BASE_IMAGE_NAME)
	# $@: $(INSTALL_COMMAND)
	#
	$(ECHO) $(INSTALL_COMMAND)
.PHONY: install-centos

install-ubuntu: INSTALL_COMMAND += -i $(UBUNTU_BASE_IMAGE_NAME),
install-ubuntu:
	#
	# $@: install vnc, novnc and xfce on $(UBUNTU_BASE_IMAGE_NAME)
	# $@: $(INSTALL_COMMAND)
	#
	$(ECHO) $(INSTALL_COMMAND)
.PHONY: install-ubuntu

install: install-ubuntu install-centos
	@echo '$(shell date) Makefile: success: $@ '
.PHONY: install

commit-centos: COMMIT_COMMAND += -e base_image_name=$(CENTOS_BASE_IMAGE_NAME)
commit-centos: COMMIT_COMMAND += -e container_image_basename=$(CONTAINER_IMAGE_BASENAME)
commit-centos: COMMIT_COMMAND += -e container_image_prefix=$(CONTAINER_IMAGE_PREFIX)
commit-centos: COMMIT_COMMAND += -e container_image_tag=:latest-centos-$(CONTAINER_ENGINE)
commit-centos: COMMIT_COMMAND += -e container_registry=$(CONTAINER_REGISTRY)
commit-centos: COMMIT_COMMAND += -e container_image_tag=:latest-centos-$(CONTAINER_ENGINE)
commit-centos:
	#
	# $@: save running image with proper tag and set metadata
	# $@: $(COMMIT_COMMAND)
	#
	$(ECHO) $(COMMIT_COMMAND)
.PHONY: commit-centos

commit-ubuntu: COMMIT_COMMAND += -e base_image_name=$(UBUNTU_BASE_IMAGE_NAME)
commit-ubuntu: COMMIT_COMMAND += -e container_image_basename=$(CONTAINER_IMAGE_BASENAME)
commit-ubuntu: COMMIT_COMMAND += -e container_image_prefix=$(CONTAINER_IMAGE_PREFIX)
commit-ubuntu: COMMIT_COMMAND += -e container_image_tag=:latest-ubuntu-$(CONTAINER_ENGINE)
commit-ubuntu: COMMIT_COMMAND += -e container_registry=$(CONTAINER_REGISTRY)
commit-ubuntu:
	#
	# $@: save running image with proper tag and set metadata
	# $@: start
	
	$(ECHO) $(COMMIT_COMMAND)
.PHONY: commit-ubuntu

commit: commit-centos commit-ubuntu
	@echo '$(shell date) Makefile: success: $@ '
.PHONY: commit  # commit base images as final images and set apropriate metadata

build-container-centos: | start-base-centos install-centos commit-centos
	@echo '$(shell date) Makefile: success: $@ '
.PHONY: build-container-centos   # build centos container image

build-container-ubuntu: | start-base-ubuntu install-ubuntu commit-ubuntu
	@echo '$(shell date) Makefile: success: $@ '
.PHONY: build-container-ubuntu   # build ubuntu container image

build-container: build-container-centos build-container-ubuntu
	@echo '$(shell date) Makefile: success: $@ '
.PHONY: build-container  # build container images

start-centos: run-centos
run-centos: RUN_COMMAND += -e desired_state=started
run-centos: RUN_COMMAND += -e container_image_tag=:latest-centos-$(CONTAINER_ENGINE)
run-centos:
	#
	# $@: $(RUN_COMMAND)
	#
	$(ECHO) $(RUN_COMMAND)
.PHONY: run-centos  # run the centos container (DEBUG=1 to enter container)

start-ubuntu: run-ubuntu
run-ubuntu: RUN_COMMAND += -e desired_state=started
run-ubuntu: RUN_COMMAND += -e container_image_tag=:latest-ubuntu-$(CONTAINER_ENGINE)
run-ubuntu:
	#
	# $@: $(RUN_COMMAND)
	#
	$(ECHO) $(RUN_COMMAND)
.PHONY: run-ubuntu  # run the ubuntu container (DEBUG=1 to enter container)

stop-centos: RUN_COMMAND += -e desired_state=absent
stop-centos: RUN_COMMAND += -e container_image_tag=:latest-centos-$(CONTAINER_ENGINE)
stop-centos:
	#
	# $@: $(RUN_COMMAND)
	#
	$(ECHO) $(RUN_COMMAND)
.PHONY: stop-centos  # stop the centos container (DEBUG=1 to enter container)

stop-ubuntu: RUN_COMMAND += -e desired_state=absent
stop-ubuntu: RUN_COMMAND += -e container_image_tag=:latest-ubuntu-$(CONTAINER_ENGINE)
stop-ubuntu:
	#
	# $@: $(RUN_COMMAND)
	#
	$(ECHO) $(RUN_COMMAND)
.PHONY: stop-ubuntu  # stop the ubuntu container (DEBUG=1 to enter container)

stop: stop-ubuntu stop-centos
	@echo '$(shell date) Makefile: success: $@ '
.PHONY: stop

clean-ubuntu: CLEAN_COMMAND += -e container_image_tag=:latest-ubuntu
clean-ubuntu:
	#
	# $@: $(CLEAN_COMMAND)
	#
	$(ECHO) $(CLEAN_COMMAND)
.PHONY: clean-ubuntu

clean-centos: CLEAN_COMMAND += -e container_image_tag=:latest-centos
clean-centos:
	#
	# $@: $(CLEAN_COMMAND)
	#
	$(ECHO) $(CLEAN_COMMAND)
.PHONY: clean-centos

clean: stop-base clean-ubuntu clean-centos
	@echo '$(shell date) Makefile: success: $@ '
.PHONY: clean  # stop running base images and remove created images

push-centos: PUSH_COMMAND += -e container_action=push
push-ubuntu: PUSH_COMMAND += -e container_image_tag=:latest-ubuntu-$(CONTAINER_ENGINE)
push-ubuntu: prepare_localhost
	#
	# $@: push container image to repository
	# $@: $(PUSH_COMMAND)
	
	$(ECHO) $(PUSH_COMMAND)
	@echo '$(shell date) Makefile: success: $@ '
.PHONY: push-ubuntu

push-centos: PUSH_COMMAND += -e container_action=push
push-centos: PUSH_COMMAND += -e container_image_tag=:latest-centos-$(CONTAINER_ENGINE)
push-centos: prepare_localhost
	#
	# $@: push container image to repository
	# $@: $(PUSH_COMMAND)
	
	$(ECHO) $(PUSH_COMMAND)
	@echo '$(shell date) Makefile: success: $@ '
.PHONY: push-centos

push: push-centos push-ubuntu
	@echo '$(shell date) Makefile: success: $@ '
.PHONY: push  # push image to $(CONTAINER_REGISTRY)

pull-centos: PUSH_COMMAND += -e container_action=pull
pull-ubuntu: PUSH_COMMAND += -e container_image_tag=:latest-ubuntu-$(CONTAINER_ENGINE)
pull-ubuntu: prepare_localhost
	#
	# $@: pull container image to repository
	# $@: $(PUSH_COMMAND)
	
	$(ECHO) $(PUSH_COMMAND)
	@echo '$(shell date) Makefile: success: $@ '
.PHONY: pull-ubuntu

pull-centos: PUSH_COMMAND += -e container_action=pull
pull-centos: PUSH_COMMAND += -e container_image_tag=:latest-centos-$(CONTAINER_ENGINE)
pull-centos: prepare_localhost
	#
	# $@: pull container image to repository
	# $@: $(PUSH_COMMAND)
	
	$(ECHO) $(PUSH_COMMAND)
	@echo '$(shell date) Makefile: success: $@ '
.PHONY: pull-centos

pull: pull-centos pull-ubuntu
	@echo '$(shell date) Makefile: success: $@ '
.PHONY: pull  # pull image from $(CONTAINER_REGISTRY)
